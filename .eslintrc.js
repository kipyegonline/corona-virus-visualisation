module.exports = {
    "extends": ["eslint:recommended", "prettier", "airbnb"],
    "env": {
        node: true,
        browser: true,
        jquery: true,
        es6: true,
        jest: true
    },
    parserOptions: {
        "ecmaVersions": 10,
        "sourceType": "modules",
        ecmaFeatures: {
            jsx: true
        }
    },
    rules: {
        "semi": [2, "always"],
        "quotes": [2, "double"],
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "warn"
    },
    plugins: ["react", "prettier", "react-hooks"]
}